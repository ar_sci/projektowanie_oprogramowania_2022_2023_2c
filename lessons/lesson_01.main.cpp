#include <iostream>
#include <vector>
#include <bitset>
#include <array>

#include <functional>
// int bool float

std::vector<int> filter(const std::vector<int>& dane);
void display(const std::vector<int>& dane);

struct Pozycja
{
    int x;
    int y;
};

int main()
{
    //Pozycja pozycja;
    std::function<void(const std::vector<int>&)> func = &display;

    display({1, 2, 3});
    func({ 3, 2, 1 });

    // snake case
    // to_jest_nazwa_funckji
    // cammel case
    // ToJestNazwaFunckji

    //consteval
    //constexpr int size = 10;
    const int size = 10;
    int tab[size];

    // 0 -> (10 - 1)
    tab[0] = 10;
    tab[0] = tab[0] + 10;
    int w = tab[0] + 10;

    std::array<int, 50> arr;
    arr[0] = 1000011;
    arr[49] = 19999;


    std::vector<int> arr_dyn;
    arr_dyn.push_back(999);
    arr_dyn.push_back(9);
    arr_dyn.push_back(81999);
    arr_dyn.push_back(7999);

    arr_dyn[1] = 888;
    int siZe = arr_dyn.size();
    //arr_dyn.clear();

    int* vec_ptr = arr_dyn.data();

    std::cout << *(vec_ptr+1);

    int l = 10;
    // addres -> index tab w pamieci
    // local memory address
    // ...
    int* ptr = &l;
    *ptr = 10;

    int sizE = 20;
    int* tab_dyn_new = new int[sizE];
    tab_dyn_new[5] = 10;
    delete[] tab_dyn_new;


    //size = 20;
    //int& size_ref = const_cast<int&>(size);
    //size_ref = 20;
    int d = -1;


    int licznik = 0;

    std::vector<int> d1;// { 1, 2, 3, 4, 5 };
    for (int i = 10; i < 20; i++) {
        d1.push_back(rand() % 1024);
    }
    std::vector<int> wynik_d1 = filter(d1);
    display(wynik_d1);
    std::cout << std::endl;

    std::vector<int> d2;
    for (int i = 10; i < 200; i++) {
        d2.push_back(rand() % 1024);
    }
    std::vector<int> wynik_d2 = filter(d2);
    display(wynik_d2);

    // ==
    // !=
    if (licznik > 5 && licznik < 10) {
        //if (licznik < 10) {
        //    ..
        //}
    }

    int wynik = licznik + 10;
    licznik += 10;

    //std::cout << "1. Wyswietl 1" << std::endl;
    //std::cout << "2. Wyswietl 2" << std::endl;
    //std::cout << "3. Wyswietl 3" << std::endl;
    //std::cout << "4. Wyswietl 4" << std::endl;
    //std::cout << "5. Wyswietl 5" << std::endl;

    for (int index = 100; index < 200; ++index) {
        std::cout << index + 1 <<". Wyswietl " << index + 1 << std::endl;
        //continue;
        //break;
    }

    for (int index = 200 - 1; index >= 0; --index) {
    }

    //if () {
    //    int d = -1;
    //}

}

std::vector<int> filter(const std::vector<int>& dane)
{
    std::vector<int> result;

    for (int i = 0; i < dane.size(); ++i) {
        int liczba = dane[i];
        if (liczba % 2 == 0) {
            result.push_back(liczba);
        }
    }

    return result;
}

void display(const std::vector<int>& dane)
{
    for (const auto& liczba : dane) {
        std::cout << liczba << ", ";
    }
    std::cout << std::endl;
}