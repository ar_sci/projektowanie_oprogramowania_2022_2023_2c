#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <limits>

template<typename T>
T finder(const std::vector<T>& tab);

template<>
int finder(const std::vector<int>& tab)
{
	int v = std::numeric_limits<int>::min();
	for (int x : tab) {
		if (x > v) {
			v = x;
		}
	}
	return v;
}

template<>
float finder<float>(const std::vector<float>& tab)
{
	float v = std::numeric_limits<float>::max();
	for (float x : tab) {
		if (x < v) {
			v = x;
		}
	}
	return v;
}

int main()
{
	std::vector<int> tab_i = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
	std::vector<float> tab_f = { 123.0f, 12.123f, 10.10f, 3.14f, 2.72f };

	std::cout << "Największa liczba całkowita:" << finder<int>(tab_i) << std::endl;
	std::cout << "Najmniejsza liczba zmiennoprzecinkowa: " << finder<float>(tab_f) << std::endl;


	return 0;
}
