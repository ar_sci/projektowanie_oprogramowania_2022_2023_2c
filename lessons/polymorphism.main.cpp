#include <iostream>
#include <string>
#include <vector>

class Base {
public:
    Base(int initial_value)
        : m_value{ initial_value }
    {
    }

    virtual ~Base() = default;

    virtual int get_number() const = 0;
    virtual void update() = 0;

protected:
    int m_value;
};

class Random : public Base {
public:
    Random(int initial_value)
        : Base{ initial_value }
    {
    }

    int get_number() const override { return m_value; }
    void update() override { m_value = rand(); }
};

class Increment : public Base {
public:
    Increment()
        : Base{ 0 }
    {
    }

    int get_number() const override { return m_value; }
    void update() override { m_value = 99; m_value += 1; }
};

int main(int, char* argv[])
{
    std::string str = "Text";
    str.size();
    str.clear();

    std::string* ptr = new std::string{ "Text" };
    ptr->clear();


    std::vector<Base*> object;

    object.push_back(new Random{ 99 });
    object.push_back(new Increment{});
    object.push_back(new Random{ 303 });
    object.push_back(new Increment{});
    object.push_back(new Increment{});

    for (const auto& obj : object) {
        std::cout << obj->get_number() << std::endl;
        obj->update();
        std::cout << obj->get_number() << std::endl;
    }

    return 0;
}
