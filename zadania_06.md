# Napisy

W pliku `zadania_06_napisy.txt` znajduje się 1000 napisów o długościach od 2 do 16 znaków, każdy napis w osobnym wierszu. W każdym napisie mogą wystąpić jedynie dwa znaki: `0` lub `1`.
Napisz program w języku `C++`, za pomocą którego uzyskasz odpowiedzi na poniższe polecenia. Odpowiedzi zapisz w pliku `zadania_06_odpowiedzi.txt`, a odpowiedź do każdego podpunktu poprzedź literą oznaczającą ten podpunkt.

a) Podaj, ile jest napisów o parzystej długości. \
b) Podaj, ile jest napisów, które zawierają taką samą liczbę zer i jedynek. \
c) Podaj, ile jest napisów składających się z samych zer, oraz podaj, ile jest napisów składających się z samych jedynek. \
d) Dla każdej liczby 2, 3, ...,16k podaj liczbę napisów o długości k znajdujących się w pliku napisy.txt, tzn. podaj, ile jest napisów 2-znakowych, ile jest napisów 3-znakowych itd.

---

W zadaniu należy wykorzystać najlepiej funkcje należące do modulu `algorithm`, tablice dynamiczne (sekwencyjne lub asocjacyjne) i inne elementy języka.

---

```
Odpowiedzi:
a)  504

b)  110

c)
    liczba napisów zawierających same zera – 32
    liczba napisów zawierających same jedynki – 50

d) 
    2-znaki: 43
    3-znaki: 38
    4-znaki: 37
    5-znaków: 57
    6-znaków: 53
    7-znaków: 68
    8-znaków: 78
    9-znaków: 103
    10-znaków: 83
    11-znaków: 90
    12-znaków: 86
    13-znaków: 81
    14-znaków: 68
    15-znaków: 59
    16-znaków: 56
```