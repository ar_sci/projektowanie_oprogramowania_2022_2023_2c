#include <iostream>
#include <vector>

namespace zadanie_0 {
    void execute();
}

namespace zadanie_1 {
    struct MinMaxResult {
        int min;
        int max;
    };

    MinMaxResult minmax(const std::vector<int>& numbers);
    int sum(const std::vector<int>& numbers);
    float mean(const std::vector<int>& numbers);
}

namespace zadanie_2 {
    struct Point {
        float x;
        float y;
    };

    struct Rectangle {
        float x;
        float y;
        float width;
        float height;
    };

    bool contains(const Rectangle& rectangle, const Point& point);
    bool intersects(const Rectangle& rectangle1, const Rectangle& rectangle2);
}

namespace zadanie_3 {
    constexpr bool is_prime_number(int n);
    void generate_prime_numbers(int n);
}

int main(int argc, char* argv[])
{
    zadanie_0::execute();
    zadanie_3::generate_prime_numbers(512);

    return 0;
}

namespace zadanie_0 {
    void execute()
    {
        std::vector<int> numbers;

        int size = 0;
        std::cout << "Ile liczb chcesz wprowadzic: ";
        std::cin >> size;

        for (int i = 0; i < size; ++i) {
            int buffor;
            std::cin >> buffor;
            numbers.push_back(buffor);
        }

        std::cout << "Wyswietlanie liczb od konca: ";
        for (int i = size - 1; i >= 0; --i) {
            std::cout << numbers[i] << " ";
        }
        std::cout << std::endl;

        const zadanie_1::MinMaxResult minmax = zadanie_1::minmax(numbers);
        std::cout << "Minimalna liczba w zbiorze to: " << minmax.min << std::endl;
        std::cout << "Maksymalna liczba w zbiorze to: " << minmax.max << std::endl;
        std::cout << "Suma liczb to: " << zadanie_1::sum(numbers) << std::endl;
        std::cout << "Srednia to: " << zadanie_1::mean(numbers) << std::endl;
    }
}

namespace zadanie_1 {
    MinMaxResult minmax(const std::vector<int>& numbers)
    {
        MinMaxResult result;
        result.min = INT_MAX;
        result.max = INT_MIN;

        for (int i = 0; i < numbers.size(); ++i) {
            if (numbers[i] > result.max) {
                result.max = numbers[i];
            }
            if (numbers[i] < result.min) {
                result.min = numbers[i];
            }
        }

        return result;
    }

    int sum(const std::vector<int>& numbers)
    {
        int result = 0;
        for (const auto& number : numbers) {
            result += number;
        }
        return result;
    }

    float mean(const std::vector<int>& numbers)
    {
        const int s = sum(numbers);
        return static_cast<float>(s) / static_cast<float>(numbers.size());
        //return (float)s / (float)numbers.size();

        // const float s = 1.0f * sum(numbers);
        // return s / number.size();
    }
}

namespace zadanie_2 {
    bool contains(const Rectangle& rectangle, const Point& point)
    {
        return true
            && point.x >= rectangle.x
            && point.x <= rectangle.x + rectangle.width
            && point.y >= rectangle.y
            && point.y <= rectangle.y + rectangle.height;
    }

    bool intersects(const Rectangle& rectangle1, const Rectangle& rectangle2)
    {
        return false
            || contains(rectangle1, Point{ rectangle2.x, rectangle2.y })
            || contains(rectangle1, Point{ rectangle2.x + rectangle2.width, rectangle2.y })
            || contains(rectangle1, Point{ rectangle2.x, rectangle2.y + rectangle2.height })
            || contains(rectangle1, Point{ rectangle2.x + rectangle2.width, rectangle2.y + rectangle2.height });
    }
}

namespace zadanie_3 {
    constexpr bool is_prime_number(int n)
    {
        for (int i = 2; i * i <= n; ++i) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    void generate_prime_numbers(int n)
    {
        std::cout << "Liczby pierwsze do liczby " << n << " to: ";
        for (int i = 2; i < n; ++i) {
            if (is_prime_number(i)) {
                std::cout << i << " ";
            }
        }
        std::cout << std::endl;
    }
}
