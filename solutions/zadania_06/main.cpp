#include <vector>
#include <algorithm>
#include <iostream>
#include <string>
#include <ranges>
#include <fstream>
#include <map>

int main(int /*argc*/, char* /*argv*/[])
{
    std::vector<std::string> data{};

    std::fstream file{ "./napisy.txt", std::ios::in };
    if (file.good()) {
        while (!file.eof()) {
            std::string line{};
            std::getline(file, line);
            data.push_back(line);
        }
        if (!data.empty()) {
            data.pop_back();
        }
    }
    file.close();

    std::cout << std::ranges::count_if(data, [](const auto& line) { return (line.size() % 2) == 0; }) << std::endl;
    std::cout << std::ranges::count_if(data, [](const auto& line) { return std::ranges::count(line, '0') == std::ranges::count(line, '1'); }) << std::endl;
    std::cout << std::ranges::count_if(data, [](const auto& line) { return std::ranges::all_of(line, [](const auto& character) { return character == '0'; }); }) << std::endl;
    std::cout << std::ranges::count_if(data, [](const auto& line) { return std::ranges::all_of(line, [](const auto& character) { return character == '1'; }); }) << std::endl;

    std::map<int, int> d{};
    for (const auto& line : data) {
        d[line.size()]++;
    }
    for (const auto& entry : d) {
        std::cout << entry.first << "-znaki: " << entry.second << std::endl;
    }

    return 0;
}
