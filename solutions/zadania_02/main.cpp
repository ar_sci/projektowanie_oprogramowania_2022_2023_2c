#include <cstdlib>
#include <iostream>

namespace tic_tac_toe {
    // zmienne globalne dla namespace `tic_tac_toe`
    const char m_cross = 'x';
    const char m_dot = 'o';
    const char m_empty = ' ';
    const unsigned int m_fields = 3;
    char** m_board = nullptr;

    // funkcje:
    void initialization();
    bool check_move(unsigned int row, unsigned int col);
    bool check_winner(const char player);
    void display_next_move(const char player);
    void input_move(unsigned int& row, unsigned int& col);
    void display_board();
    void display_winner(const char player);
    char player_next(const char player);
    void launch();
    void reset();
    void termination();
}

int main(int /*argc*/, char* /*argv*/[])
{
    tic_tac_toe::initialization();
    bool is_running = true;
    do {
        tic_tac_toe::reset();
        tic_tac_toe::launch();

        std::cout << "Czy chcesz zagrac jeszcze raz?" << std::endl;
        std::cout << "Wpisz T(tak)/N(nie): ";
        char next;
        std::cin >> next;
        is_running = 'T' == next;
    } while (is_running);
    tic_tac_toe::termination();

    return 0;
}

namespace tic_tac_toe {
    void initialization()
    {
        m_board = new char*[3];
        for (int i = 0; i < 3; i++) {
            m_board[i] = new char[3];
        }
    }

    bool check_move(unsigned int row, unsigned int col)
    {
        const bool is_valid = true
            && row < m_fields
            && col < m_fields
            && m_board[row][col] == m_empty;
        return is_valid;
    }

    bool check_winner(const char player)
    {
        for (int row = 0; row < m_fields; ++row) {
            bool isWinner = true;
            for (int col = 0; col < m_fields; ++col) {
                if (m_board[row][col] != player) {
                    isWinner = false;
                    break;
                }
            }
            if (isWinner) {
                return true;
            }
        }

        for (int col = 0; col < m_fields; ++col) {
            bool isWinner = true;
            for (int row = 0; row < m_fields; ++row) {
                if (m_board[row][col] != player) {
                    isWinner = false;
                    break;
                }
            }
            if (isWinner) {
                return true;
            }
        }

        if (m_board[0][0] == player && m_board[1][1] == player && m_board[2][2] == player) {
            return true;
        }

        if (m_board[2][0] == player && m_board[1][1] == player && m_board[0][2] == player) {
            return true;
        }
        return false;
    }

    void launch()
    {
        char current_player = m_cross;
        bool is_winner = false;
        do {
            display_next_move(current_player);

            unsigned int row, col;
            input_move(row, col);
            if (check_move(row - 1, col - 1)) {
                m_board[row - 1][col - 1] = current_player;
                display_board();
                if (check_winner(current_player)) {
                    display_winner(current_player);
                    break;
                }
                current_player = player_next(current_player);
            }
            else {
                std::cout << "Ruch nie jest prawidlowy, prosze powtorzyc!" << std::endl;
            }
        } while (!is_winner);
    }

    void display_next_move(const char player)
    {
        std::cout << "Ruch gracza: " << player << std::endl;
    }

    void input_move(unsigned int& row, unsigned int& col)
    {
        std::cout << "Wprowadz wiersz [" << 1 << " -" << m_fields << "]: ";
        std::cin >> row;
        std::cout << "Wprowadz kolumne [" << 1 << " -" << m_fields << "]: ";
        std::cin >> col;
    }

    void display_board()
    {
        for (int row = 0; row < m_fields; row++) {
            for (int col = 0; col < m_fields; col++) {
                std::cout << m_board[row][col];
            }
            std::cout << std::endl;
        }
    }

    void display_winner(const char player)
    {
        std::cout << "Gracz: "<< player << " jest winnerem!!!" << std::endl;
    }

    char player_next(const char player)
    {
        if (player == m_cross) {
            return m_dot;
        }
        else {
            return m_cross;
        }
    }

    void reset()
    {
        for (int row = 0; row < m_fields; row++) {
            for (int col = 0; col < m_fields; col++) {
                m_board[row][col] = m_empty;
            }
        }
    }

    void termination()
    {
        for (int i = 0; i < m_fields; i++) {
            delete[] m_board[i];
        }
        delete[] m_board;
     }
}
