#include <iostream>
#include <vector>
#include <set>
#include <map>

namespace zadanie_1 {
    std::vector<int> execute(int number = 1024);
}

namespace zadanie_2 {
    std::vector<int> execute(const std::vector<int>& source, const std::vector<int>& values);
}

namespace zadanie_3 {
    struct result_t {
        std::vector<int> d1;
        std::map<int, int> d2;
    };
    result_t execute(const std::vector<int>& vector);
}


int main(int argc, char* argv[])
{
    srand(3388);

    // Pamiętaj, wynik funkcji może być pominięty, ale jeśli nie przypiszesz danych do zmiennej, to one znikną, bo nie są potrzebne
    const std::vector<int> big_set = zadanie_1::execute(99999);
    const std::vector<int> low_set = zadanie_1::execute(10);

    const std::vector<int> indexes = zadanie_2::execute(big_set, low_set);

    zadanie_3::result_t result = zadanie_3::execute(big_set);

    for (const auto& uniqueValue : result.d1) {
        std::cout << uniqueValue << ", ";
    }
    std::cout << std::endl;

    system("pause");

    // jest kilka sposobów na wyświetlanie danych z mapy:
    // automatycznie typy danych są przypisane do zmiennych
    // pamiętaj, że nazwy zmiennych możesz zmienic (aby były odpowiednie do zastosowania)
    // np: value, counter
    // key => wartość liczbowa, która występuje w zbiorze
    // value => licznik, który informuje ile razy dana liczba wystąpiła
    for (const auto& [key, value] : result.d2) {
        std::cout << "Wartosc: " << key << " powtorzyla sie: " << value << " razy." << std::endl;
    }
}


namespace zadanie_1 {
    // Definicja funkcji -> czyli określa co ona ma robić
    // Przyjmuje jeden argument, liczbę elementów do wygenerowania
    // Tworzymy zmienną, która będzie przechowywać wygenerowane elementy
    // Funkcja zwraca wynik w postaci dynamicznej tablicy
    std::vector<int> execute(int number)
    {
        std::vector<int> result;
        result.reserve(number); // jeśli znasz liczbę elementów, możesz zarezerwować pamięć w tablicy dynamicznej

        for (int i = 0; i < number; ++i) {
            const int value = rand();
            result.push_back(value);
        }

        return result;
    }
}

namespace zadanie_2 {
    // Funkcja ta, ma za zadanie wyszukać indexy dla szukanych wartości
    // pamiętaj, że jeśli pracujemy na danych określonymi złożonymi typami, pamiętaj o `const ref`
    // tak jak w tym przypadku: const {data_type}& {variable_name}
    std::vector<int> execute(const std::vector<int>& source, const std::vector<int>& values)
    {
        std::vector<int> result;
        result.reserve(values.size()); // znamy ilość wynikwej tablicy, ponieważ szukamy indeksów, które znajdują się w pierwszej tablicy

        // Pamiętaj, że możesz tworzyć nazwy, tak aby wiedzieć co kryją (po prostu łatwiej, jest później analizować)
        for (int valuesIndex = 0; valuesIndex < values.size(); ++valuesIndex) {
            // przypisuje sobie wartość z tablicy
            // ta wartość jest szukana w pierwszej tablicy
            const int findingValue = values[valuesIndex];

            // zmienna potrzebna do określenia, czy wartość została znaleziona w pierwszym zbiorze liczb
            int foundIndex = -1;
            for (int sourceIndex = 0; sourceIndex < source.size(); ++sourceIndex) {
                // sprawdzamy czy w pierwszej tablicy występuje szukana wartość z drugiego zbioru
                if (findingValue == source[sourceIndex]) {
                    // jeśli tak, to wtedy przypisujemy wyszukany index do zmiennej
                    foundIndex = sourceIndex;
                    // musimy zakończyć działanie pętli, ponieważ chcemy uzyskać pierwszy index wyszukanej liczby
                    break;
                }
            }

            // wynik szukania dodajemy do tablicy wynikowej
            result.push_back(foundIndex);
        }

        // zwracamy wynik działania funkcji
        return result;
    }
}

namespace zadanie_3 {
    result_t execute(const std::vector<int>& vector)
    {
        result_t result;

        // tworzymy strukturę danych, która zapewnia nas, że w zbiorze będą tylko unikatowe wartości
        // drugą cechą tego zbioru, jest to, że wartości są posortowane od min do max
        std::set<int> uniqueValues;
        for (int vectorIndex = 0; vectorIndex < vector.size(); ++vectorIndex) {
            const int vectorValue = vector[vectorIndex];
            uniqueValues.insert(vectorValue);
        }

        // Interesują nas tylko wartości od max do min, więc znając cechy zbioru (std::set)
        // możemy wykorzystać wsteczny iterator
        // więc `std::set<int>::const_reverse_iterator`, typ danych mówiący o iteratorze
        // który z nazwy mówi nam, że jest stały, nie wolno nam modyfikować wartości
        // dodatkowo zawiera `reverse`, który mówi, że będziemy iterować od końca naszego zbioru
        // `crbegin` => metoda, która zwraca nam iterator, tak abyśmy mogli iterować od ostaniego elementu do pierwszego
        // dla iteratorów nie używamy operatorów > lub <
        // zazwyczaj sprawdzamy czy jest różny od końcowego iteratora w zbiorze
        // jak dla indeksów, dodajemy zawsze wartość +1 za pomocą pre lub post inkrementacji
        for (
            std::set<int>::const_reverse_iterator it = uniqueValues.crbegin();
            it != uniqueValues.crend();
            ++it) {
            result.d1.push_back(*it);
        }


        // Pamiętaj, że powyższa pętla może zostać powtórzona, więc 2 razy będziemy przechodzić przez wszystkie elementy w tablicy
        // nazwy zmiennych mogą się powtarzać, ponieważ znajduja się w osobnych zakresach (scope)
        // czyli poprostu w {}

        // zliczamy ile razy powtórzą się wartości w zbiorze
        for (int vectorIndex = 0; vectorIndex < vector.size(); ++vectorIndex) {
            // pobieramy wartość z tablicy
            const int vectorValue = vector[vectorIndex];

            // używamy struktury danych, która posiada klucz i wartość
            // w tym momecie tworzymy wartość w strukturze, jeśli jej tam nie ma
            // jeśli ona tam już jest, to dodajemy do niej wartość 1
            // jeśli jej nie ma, to zostanie utworzony wpis w mapie,
            // np. { 50, 0 }
            // pamiętaj, że jeśli nie ma wpisu, to zostanie on utworzony, a wartość zostanie domyślnie zainicjalizowana
            // dla typów bazowych jest to zawsze 0
            // dla bool => false
            // dla char = '';
            // dla std::vector => pusta tablica
            // i tak dalej
            result.d2[vectorValue] += 1;
        }

        return result;
    }
}
