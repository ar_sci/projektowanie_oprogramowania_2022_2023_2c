#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

const std::vector<std::string> words = {
    "dog", "tiger", "count", "cereal", "remember", "X-ray", "stomach", "climate", "pyramid", "exclude", "lineage", "deserve", "verdict", "racism", "update", "source", "cottage", "exact", "bald", "preach", "master", "overview", "climb", "promote", "stereotype", "coerce", "decay", "superintendent", "write", "jet", "do", "overall", "youth", "fountain", "shop", "tax", "cunning", "consider", "trust", "heel", "reduction", "wagon", "carve", "variant", "ordinary", "archive", "work", "wedding", "hover", "drill",
    "series", "complain", "drawing", "photocopy", "funeral", "depression", "tray", "repeat", "emphasis", "wage", "agony", "function", "thigh", "wheel", "passage", "budge", "school", "zone", "outer", "presence", "cream", "torture", "market", "clear", "offset", "degree", "crutch", "census", "gene", "adventure", "toast", "to", "build", "contrary", "dedicate", "hurt", "test", "past", "despise", "lazy", "certain", "ambiguous", "kinship", "attention", "return", "glance", "tail", "minister", "assignment", "ordinary",
    "consider", "filter", "weed", "judge", "profit", "trick", "define", "period", "launch", "umbrella", "reliance", "straighten", "business", "unfortunate", "cottage", "tiger", "sound", "generation", "scramble", "shop", "useful", "manufacturer", "wound", "onion", "jealous", "color-blind", "rung", "degree", "few", "licence", "quality", "dish", "remark", "spit", "addicted", "slow", "connection", "parking", "breathe", "will", "nut", "row", "rain", "hen", "detective", "sick", "bait", "digress", "mail", "embryo",
};

struct data_t {
    std::size_t id;
    std::string word;
    std::size_t number;
};

int main(int, char* argv[])
{
    srand(44338);
    std::vector<data_t> data;

    for (std::size_t i = 0; i < 100000; ++i) {
        data.push_back({
            i,
            words[rand() % words.size()],
            static_cast<std::size_t>(rand()),
        });
    }

    std::size_t task_1_tray = std::count_if(data.cbegin(), data.cend(), [](const data_t& element) {
        return element.word == "tray";
    });
    std::size_t task_1_business = std::count_if(data.cbegin(), data.cend(), [](const data_t& element) {
        return element.word == "business";
    });
    std::size_t task_1_budge = std::count_if(data.cbegin(), data.cend(), [](const data_t& element) {
        return element.word == "budge";
    });

    std::cout << "Zadanie 1:" << std::endl;
    std::cout << "\ttray: " << task_1_tray << std::endl;
    std::cout << "\tbusiness: " << task_1_business << std::endl;
    std::cout << "\tbudge: " << task_1_budge << std::endl;

    const auto it_x_ray = std::find_if(data.cbegin(), data.cend(), [](const data_t& element) {
        return element.word == "X-ray" && element.id > 25000;
    });
    const auto it_trick = std::find_if(data.cbegin(), data.cend(), [](const data_t& element) {
        return element.word == "trick" && element.id > 45000;
    });

    std::cout << "Zadanie 2:" << std::endl;
    std::cout << "\tX-ray: " << it_x_ray->id << " index: " << std::distance(data.cbegin(), it_x_ray) << std::endl;
    std::cout << "\ttrick: " << it_trick->id << " index: " << std::distance(data.cbegin(), it_trick) << std::endl;

    std::sort(data.begin(), data.end(), [](const data_t& lhs, const data_t& rhs) {
        return lhs.word < rhs.word;
    });

    std::cout << "Zadanie 3:" << std::endl;
    for (std::size_t i = 0; i < 10; ++i) {
        std::cout
            << "\tid: " << data[i].id
            << " word: " << data[i].word
            << " number: " << data[i].number
            << std::endl;
    }

    const bool test = std::all_of(data.cbegin(), data.cend(), [](const data_t& element) {
        return element.number > 0;
    });
    std::cout << "Czy wszystkie liczby sa wieksze od 0: "<< (test ? "tak" : "nie") << std::endl;

    return 0;
}
