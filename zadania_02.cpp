#include <cstdlib>
#include <iostream>

namespace tic_tac_toe {
    // zmienne globalne dla namespace `tic_tac_toe`
    const char m_cross = 'x';
    const char m_dot = 'o';
    const char m_empty = ' ';
    const unsigned int m_fields = 3;
    char** m_board = nullptr;

    // funkcje:
    void initialization();
    bool check_move(unsigned int row, unsigned int col);
    bool check_winner(const char player);
    void display_next_move(const char player);
    void input_move(unsigned int & row, unsigned int & col);
    void display_board();
    void display_winner(const char player);
    char player_next(const char player);
    void launch();
    void reset();
    void termination();
}

int main(int /*argc*/, char * /*argv*/[])
{
    /**
     * tic_tac_toe::initialization(); => wywolanie funckji z namespace, ktora utworzy tablice 2 wymiarowa
     * is_running => zmienna, ktora jest potrzebna do sprawdzenia, czy petla z uruchomiona aplikacja ma ciagle dzialac (glownie, czy rozpoczac nastepna gre)
     * petla `do {} while('warunek')` => petla, ktora wykonuje pierw iteracje, a pozniej sprawdza warunek (petla `for` lub `while`, najpierw sprawdza warunek, pozniej wykonuje iteracje)
     * tic_tac_toe::reset(); => wywolanie funckji, ktora zresetuje plansze gry (podczas alokacji, wskazniki wskazuja na śmieci, więc warto wyzerowac nasza tablice, aktualnie funkcja ma nadpisac kazdy znak na spacje)
     * tic_tac_toe::launch(); => wywolanie gry, ktora posiada wlasna petle (petla rozgrywki)
     * INFO: nastepne polecenia, to juz powinienes znac (wyswietlenie tekstu na ekranie, pobranie od uzytkownika znaku, sprawdzenie znaku (czy jest to litera T))
     * INFO: pozniej sprawdzenie warunku petli, jesli zmienna `is_running` jest true, to wtedy petla wykona sie jeszcze raz, jak nie, to zostanie zakonczona
     * tic_tac_toe::termination(); => wywolanie funkcji, ktora posprzata pamiec, ktora zostala zaalokowana
     */
    tic_tac_toe::initialization();
    bool is_running = true;
    do {
        tic_tac_toe::reset();
        tic_tac_toe::launch();

        std::cout << "Czy chcesz zagrac jeszcze raz?" << std::endl;
        std::cout << "Wpisz T(tak)/N(nie): ";
        char next;
        std::cin >> next;
        is_running = 'T' == next;
    } while (is_running);
    tic_tac_toe::termination();

    return 0;
}

namespace tic_tac_toe {
    void initialization()
    {
        // TODO: utworzenie tablicy wskaznikow (rozmiar: m_fields)
        // TODO: implementacja petli, ktora utworzy tablice znakow char (rozmiar: m_fields)
        // INFO: tablica bedzie miala wymiary 3x3 = 9 elementow, tak jak to ma plansza do gry w kolko i krzyzyk
    }

    bool check_move(unsigned int row, unsigned int col)
    {
        // TODO: implementacja sprawdzenia czy zmienna row i col nie sa wieksze niz rozmiar tablicy, w tym przypadku (m_fields)
        // TODO: implementacja sprawdzajaca czy pole jest puste (m_empty)
        // INFO: funkcja zwraca wartosc `true` tylko kiedy powyzsze warunki sa sprawdzone
        // INFO: w przeciwnym razie jest to false
        bool is_valid = false;
        return is_valid;
    }

    bool check_winner(const char player)
    {
        // TODO: implementacja sprawdzajaca czy dany gracz wygral
        // INFO: ile jest kombinacji? 3 wiersze 3 kolumny 2 przekatne
        // INFO: sprawdzacie pierw kolumny lub wiersze
        // INFO: sprawdzacie przekatne
        // INFO: 
        /**
         * Plansza:
         * xox
         * oxo
         * x x
         */
        // INFO: wiec musicie sumowac i sprawdzac czy liczba wystapionych znakow gracza wynosi liczbie dostepnych pol (m_fields)
        bool is_win = false;
        return is_win;
    }

    void launch()
    {
        // INFO: current_player => zmienna, ktora okresla ktory gracz ma zaczac gre oraz przechowuje informacje kto ma zrobic ruch
        // INFO: is_winner => zmienna, ktora przechowuje true/false, czyli potrzebna informacja do sprawdzania czy petla `do while` ma wywolac kolejna iteracje
        char current_player = m_cross;
        bool is_winner = false;
        do {
            // TODO: implementacja:
            /**
             * wywolanie funkcji, ktora wyswietla informacje o nastepnym ruchu gracza
             * utworzenie zmiennych, ktore beda przechowywac informacje o nastepnym ruchu
             * wywolanie funkcji, ktora prosi uzytkownika o wprowadzenie ruchu gracza
             * INFO: pamietaj, ze gracz wpisze zakres od 1 do m_fields, a tablice dzialaja od 0, wiec musisz odjac -1 od kazdych wspolrzednych
             * sprawdz warunek czy ruch jest mozliwy do wykonania
             * INFO: jesli ruch jest bledny (wychodzi po za zakres indeksow, lub pole jest juz zajete), wyswietl odpowiedni komunika oraz wywolaj slowo kluczowe, ktore przerywa dzialanie petli, ale wykonuje kolejna iteracje
             * po warunku przypisz do planszy ruch gracza
             * wywolaj funkcje, ktora wyswietli stan planszy
             * sprawdz czy gracz wygral
             * INFO: jesli tak to wyswietl odpowiedni komunikat (wywolanie odpowiedniej funkcji)
             * INFO: jesli nie to zmien gracza (wywolanie odpowiedniej funkcji oraz przypisanie wyniku do zmiennej: `current_player`
             */
        } while (!is_winner);
    }

    void display_next_move(const char player)
    {
        // TODO: implementacja wyswietlenia informacji, ktory gracz ma aktualnie wprowadzic wspolrzedne
    }

    void input_move(unsigned int & row, unsigned int & col)
    {
        // TODO: implementacja funkcji, która wyswietal odpowiedni komunikat (`Wprowadz wiersz [zakres]: `)
        // TODO: implementacja wprowadzenia danych (std::cin), poprzez wykorzystanie referencji
        // INFO: powyzsze elementy musza byc zaimplementowane dla wprowadzenia wiersza oraz kolumny
        // INFO: chcialbym aby gracz wprowadzal zakres od 1 do 3 (m_fields)
    }

    void display_board()
    {
        // TODO: implementacja funkcji, która wyswietla aktualny stan planszy
        // INFO: 
        /**
         * Plansza:
         * xox
         * oxo
         * x x
         */
    }

    void display_winner(const char player)
    {
        // TODO: implementacja funkcji, ktora wyswietla napis, ktory gracz wygral rozgrywke
    }

    char player_next(const char player)
    {
        // TODO: implementacja funkcji, która bedzie zwracac nastepnego gracza
        // INFO: jesli player to 'x' => to funkcja zwraca gracza 'o'
        // INFO: jesli player to 'o' => to funkcja zwraca gracza 'x'
        char next = m_empty;
        return next;
    }

    void reset()
    {
        // TODO: implementacja funkcji, która resetuje plansze
        // INFO: wypelnia wszystkie elementy tablic dynamicznej elementem (m_empty)
    }

    void termination()
    {
        // TODO: implementacja funkcji, ktora usuwa tablice z pamieci
    }
}
