# Zadanie_07 - SFML_1

Tutoriale:
- [Sprites and textures](https://www.sfml-dev.org/tutorials/2.5/graphics-sprite.php)
- [Events](https://www.sfml-dev.org/tutorials/2.5/window-events.php)
- [Handling time](https://www.sfml-dev.org/tutorials/2.5/system-time.php)
- [Controlling the 2D camera with views](https://www.sfml-dev.org/tutorials/2.5/graphics-view.php)

`po_project` jest to katalog, który zawiera projekt stworzony za pomocą `CMake`. Zawiera on już bibliotekę SFML. Należy pamiętać, że projekt jest pobierany za pomocą narzędzia `git`. Tworzenie solucji dla visual studio należy wykorzystać aplikację CMake.

Projekt zawiera plik `main.cpp` w którym znajduje się implementacja tworzenie okna, obsługa prostego eventu (zamknięcie okna) oraz obsługa wyświetlania elementów.

- `render_window ` -> jest to zmienna typu `sf::RenderWindow`, która odpowiada za okno aplikacji
- `clock` -> jest to zmienna typu `sf::Clock`, który odpowiada za zliczanie czasu (zlicza ile nanosekund upłynęło od ostatniego restartu)
- `event` -> jest to zmienna typu `sf::Event`, która odpowiada za informacje, które okno otrzymało od systemu operacyjnego (czyli, otrzymuje zdarzenia np. o zmianie rozmiaru okna, o utracie/uzyskaniu aktywności okna, o naciśnięciu/puszczeniu klawisza klawiatury)
- `const sf::Time delta_time = clock.restart();` -> ta linia, resetuje zegar oraz jesteśmy wstanie uzyskać czas, który upłynął od ostatniego restartu. `delta_time` -> głównie służy do uzyskania płynności animacji grafiki na ekranie.
- `render_window.clear();` -> odpowiada za czyszczenie całego okna, w którym rysujemy różne kształty/obrazki.
- `render_window.display();` -> wymuszenie, aby okno wyświetliło cały obraz w oknie systemowym.

---
## Zadanie_0 - wczytanie tekstury
- za utworzeniem okna należy utworzyć zmienną o typie `sf::Texture`.
- na utworzonej zmiennej należy wywołać metodę `loadFromFile`, która z katalogu `assets` wczyta teksturę o nazwie `gem.png`
- sprawdź na podglądzie, czy powyższa metoda zwraca jakieś dane (jeśli, tak to zastanów się, czy jesteś wstanie je obsłużyć i jak)

## Zadanie_1 - utworzenie obiektu graficznego
- tekstura to tylko informacja o pikselach, jak one są ułożone, jaki jest rozmiar obrazka
- wyświetlanie elementów odbywa się za pomocą obiektu o typie `sf::Sprite`
- utwórz zmienną, która będzie reprezentowała obiekt do wyświetlenia
- na utworzonym obiekcie należy wywołać metodę `setTexture`, która umożliwi przypisanie obrazu do elementu wyświetlającego

## Zadanie_2 - wyświetlanie
- znajdź fragment kodu, który czyści okno oraz wymusza rysowanie nowego obrazu w oknie systemowym
- pomiędzy tymi liniami, należy odwołać się do okna w którym chciałbyś wyrysować element
- za pomocą metody `draw` na zmiennej odpowiadającej za okno, będziesz mógł wyświetlić obraz
- sprawdź rezultat budując i uruchamiając aplikację

## Zadanie_3 - pozycjonowanie / kolorowanie
- niektóre tekstury mogą być tak utworzone, żeby można było dodać kolor (taki zabieg nazywa się blendowaniem)
- znajdź linię, która odpowiada za przypisanie tekstury do elementu
- dopisz nowe wywołanie metody `setColor`, która pozwoli na dodanie koloru do tekstury
- `sf::Color` jest to typ danych, który umożliwia przekazanie informacji o kolorze (pamiętaj, że kolor składa się z 3-4 komponentów, np. RGBA)
    - biblioteka dostarcza predefiniowane kolory, możesz odwołać się do typu `sf::Color`, aby użyć koloru `sf::Color::Red` lub `sf::Color::Yellow`)
- sprawdź rezultat budując i uruchamiając aplikację
- podczas tworzenia elementu, możesz opisać jego nową pozycję, która zostanie wykorzystana, aby przetransformować element
- wykorzystaj metodę `setPosition`, do której przekażesz argumenty opisujące `x` i `y`
- PAMIETAJ, że oś Y jest odwrotna niż w matematyce

## Zadanie_4 - kolejny diament
- utwórz nowy element wyświetlający teksturę (tekstury nie musisz ładować ponownie do pamięci, ponieważ jest ona wczytana)
- ustaw inny kolor niż poprzedni element
- ustaw inną pozycję niż poprzedni element

## Zadanie_5 - utwórz własny obiekt (klasę)
- utwórz w katalogu `source` pliki `gem.hpp` oraz `gem.cpp`
- dodaj te nazwy tych plików do pliku `CMakeSources.cmake`
- PAMIĘTAJ, że po modyfikacji plików `CMake` należy wygenerować od nowa solucję (wystarczy odpalić aplikację `CMake`, która umożliwi tobie przegenerowanie, przycisk `Generate`, nie musisz zamykać Visual Studio, pamiętaj aby przeładować Solucję, wyskoczy okno w aplikacji Visual Studio)
- po dodaniu plików, należy utworzyć deklarację oraz definicję klasy `Gem`
  - pamiętaj, że w pliku nagłówkowym należy dodać `header guard`:
    ```cpp
    #ifndef PO_PROJECT_GEM_HPP
    #define PO_PROJECT_GEM_HPP

    #endif // PO_PROJECT_GEM_HPP
    ```
- w tej klasie, należy utworzyć następujące elementy:
  - `konstruktor`, posiada 2 argumenty (const sf::Texture& texture, const sf::Color& color)
  - publiczną metodę: `update`, posiada 1 argument (const sf::Time& delta_time)
  - publiczną metodę: `draw`, posiada 1 argument (const sf::RenderWindow& render_window)
  - prywatne pole: `sf::Sprite`, który będzie odpowiadał ze element do rysowania
- w pliku nagłówkowym pamiętaj o zaincludowaniu pliku nagłówkowego
  - w ciele konstruktora, należy przypisać teksturę oraz kolor do elementu `sprite`
  - w metodzie `draw`, wykorzystać argument okna i wyrysować element `sprite` na oknie
- utwórz obiekt w funkcji `main`, pamiętaj aby w trakcie tworzenia obiektu przekazać parametry, a następnie na obiekcie wywołać metodę `draw` w miejscu, gdzie wyswietlasz inne obiekty

## Zadanie_6 - aktualizacja
- obiekt posiada metodę `update`
- w definicji tej metody utwórz zmienną typu `float`, która będzie określać prędkość
- utwórz kolejną zmienną, która będzie przechowywała nowy wynik obliczeń związany z obrotem obiektu
- aktualny obrót `sprite` możesz uzyskać wywołując metodę `getRotation`
- wzór na nowy kąt obrotu: stara_wartość_obrotu + prędkość * delta_time.asSeconds()
- przypisanie nowej wartości kątu obrotu polega na wywołaniu metody `setRotation`
- w funkcji main wywołaj metodę `update` tam, gdzie jest wyliczany nowy średni czas

## Zadanie_7 - kontener diamentów
- zadanie polega na utworzeniu tablicy dynamicznej (`std::vector<Gem>`), która będzie znajdowała się w funkcji main
- należy wygenerować za pomocą pętli `for` 20 obiektów, które zostaną dodane do tej pętli
- w głównej pętli programu należy wywołać metodę `update` na wszystkich obiektach w kontenerze oraz je wyświetlić
- PS. dodaj nową metodę, która umożliwi pozycjonowanie elementów w oknie (nowa metoda do klasy Gem, też może być nazwana setPosition, która wywoła metodę setPosition na sprite w klasie)
- w trakcie tworzenia elementów, należy wylosować nowe pozycje dla diamentów za pomocą funkcji `rand`

## Zadanie_8 - nowa klasa (Tile), wczytywanie pliku
- należy dodać nową klasę, która będzie odpowiadała jednemu elementowi bloku
  - wystarczy, że klasa będzie posiadała następujące elementy: konstruktor, sprite, metodę draw, metodę setPosition
- w funkcji main należy wczytać plik `level_1.txt`, z którego zostanie wygenerowany poziom
  - w pierszej linii znajdują się wartości określające rozmiar planszy
  - w następnych znajdują się dane, które należy interpretować w następujący sposób:
    - `0` - nic się nie dzieje
    - `b` - jest generowany blok
    - `p` - jest generowany blok (platform)
    - `g` - jest generowany diament
- należy dostosować algorytm, który wygeneruje następujący widok (możliwe, że u mnie został zmodyfikowany punk środkowy, metoda setOrigin):
  ![](./assets/task_07_preview.png)
  