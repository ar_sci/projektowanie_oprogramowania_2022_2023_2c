# sci_projektowanie_oprogramowania_2022_2023_2c

Lista zadań:
1. Wyświetlanie/pobieranie danych z terminala.
1. Utworzenie struktury danych oraz implementacja funkcji: minmax, suma, średnia.
1. Utworzenie dwóch struktur danych oraz implementacja funkcji: zawiera, przecina.
1. Implementacja funkcji: czy jest liczbą pierwszą, wygeneruj liczby pierwsze.

Zadania należy zaimplementować w języku programowania c++. Każde zadanie ma znajdować się w osobnej przestrzeni nazw `namespace`. Wszystkie implementacje mają znajdować się w jednym pliku `main.cpp`. 

Przykład użycia przestrzeni nazw:
```c++
namespace ns1 {
    void foo();
}

int main()
{
    ns1::foo();
}

namespace ns1 {
    void foo()
    {
        std::cout << "Wywolanie funkcji foo" << std::endl;
    }
}
```

## Szczegóły zadań:
---
### Zadanie_1
- zaincluduj plik nagłówkowy kontenera, który umożliwi tobie przechowywanie danych w tablicy dynamicznej: `vector`
- utwórz funkcję w przestrzeni nazw o nazwie `execute` (pamiętaj o deklaracji i definicji)
- utwórz zmienną typu std::vector, która będzie mógła przechowywać liczby całkowite => `std::vector<int>`
- utwórz zmienną typu całkowitego, która będzie przechowywała rozmiar podany od użytkownika (liczba ta będzie również informowała ile razy dany użytkownik będzie chciał wprowadzić różne liczby)
- utwórz pętlę `for`, która umożliwia wprowadzenie liczb do kontenera (za pomocą metody `push_back`)
- utwórz kolejną pętlę `for`, która umożliwi wyświetlenie wprowadzonych liczb do kontera, lecz w kolejności od ostatniej do pierwszej wprowadzonej wartości

### Zadanie_2
- utwórz kolejną przestrzeń nazw
- utwórz strukturę danych o nazwie `MinMaxResult`, która będzie posiadała dwa pola/członków/membery
    - pola te, mają być typu licz całkowitych
    - nazwy to: min, max
- zaimplementuj funkcję `minmax`, która zwraca dane typu `MinMaxResult` (zamiast typu void), oraz przyjmuje argument w postaci kontenera liczb całkowitych `std::vector<int>`
    - pamiętaj, że typy złożone takie jak kontener powinno się przekazywać za pomocą stałej referencji tak, aby obiekt nie był kopiowany => `const std::vector<int>&`
- w definicji funkcji utwórz obiekt typu `MinMaxResult`, czyli twojej struktury
    - do pól dostaniesz się za pomocą operatora `.` => czyli użyjesz nazwy zmiennej, a później operator `.`
    - do pola `min`, przypisz wartość `INT_MAX`
    - do pola `max`, przypisz wartość `INT_MIN`
- za pomocą pętli `for` sprawdź czy wartość jest większa lub mniejsza od pól min i max tak, aby zaimplementować własną funkcję szukającą minimalnej i maksymalnej wartości.
- następnie zaimplementuj w tej samej przestrzeni nazw kolejne funkcje
    - `sum` => liczącą sumę wszystkich elementów znajdujących się w konterze (tak, to jest argument funkcji)
    - `mean` => śrędnią wartość znajdującą się w konterze (też jest to argument funkcji)
- finalnie zmodyfikuj działanie funkcji z zadania 1 tak, aby wyświetlała ona dodatkowe komunikaty (użyj funkcji z zadania 2):
    - `Minimalna liczba w zbiorze to:`
    - `Maksymalna liczba w zbiorze to: `
    - `Suma liczb to: `
    - `Srednia to: `


### Zadanie_3
- utwórz kolejną przestrzeń nazw
- utwórz nowe struktury:
    - Point (zwiera 2 pola typu zminnoprzecinkowego => `float`)
        - x, y
    - Rectangle (zawiera 4 pola typu `float`)
        - x, y, width, height
- zadeklaruj dwie funkcje:
    - `contains`
        - zwraca typ `bool` => true/false
        - przyjmuje dwa argumenty
            - typ `Rectangle`
            - typ `Point`
    - `intersects`
        - zwraca typ `bool` => true/false
        - przyjmuje dwa argumenty
            - typ `Rectangle`
            - typ `Rectangle`
- zdefiniuj działanie funkcji:
    - `contains` => sprawdza czy punkt znajduje się w zdeklarowanym prostokącie
    - `intersects` => sprawdza czy prostokąty się przecinają
- dla ułatwienia matematyki, zastosuj następujący schemat:
    ![alt text](assets/zadania_00_001.png)

### Zadanie_4
- utwórz kolejną przestrzeń nazw
- zadeklaruj dwie funkcje:
    - `is_prime_number`
        - zwraca typ danych w wartościach: true/false
        - przujmuje jeden argument, liczba całkowita
    - `generate_prime_numbers`
        - nie zwraca żadnych informacji
        - przujmuje jeden argument, liczba całkowita
- zdefiniuj funkcje tak, aby wykonały następujące zadania
    - `is_prime_number`, sprawdza czy argument (liczba) jest liczbą pierwszą (jeśli tak, to zwraca `true`, w przeciwnym razie `false`)
    - `generate_prime_numbers`, przyjmuje argument (liczba), za pomocą pętli `for`, zaczynając od liczby `2` do liczby `n`, sprawdza, czy każda liczba może być liczbą pierwszą, jeśli tak, to wyświetl ją w terminalu

---

Przykładowy log z działania aplikacji:
```
Ile liczb chcesz wprowadzic: 6
11
111
222
333
44
555
Wyswietlanie liczb od konca: 555 44 333 222 111 11
Minimalna liczba w zbiorze to: 11
Maksymalna liczba w zbiorze to: 555
Suma liczb to: 1276
Srednia to: 212.667
Liczby pierwsze do liczby 512 to: 2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97 101 103 107 109 113 127 131 137 139 149 151 157 163 167 173 179 181 191 193 197 199 211 223 227 229 233 239 241 251 257 263 269 271 277 281 283 293 307 311 313 317 331 337 347 349 353 359 367 373 379 383 389 397 401 409 419 421 431 433 439 443 449 457 461 463 467 479 487 491 499 503 509
```
