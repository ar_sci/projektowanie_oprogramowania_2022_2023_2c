- każde zadnaie używamy w namespace

---

## Zadanie_1
- implementacja funkcji, która zwraca tablicę dynamiczną
- losujemy liczby za pomocą `rand` => ziarno ustaw na `3388`
- funkcja przyjmuje jeden argument, jest to liczba odpowiadająca za ilość wygenerowanych elementów
    - domyślną wartościa argumentu było 1024


---
## Zadanie_2
- implementacja funkcji, która zwraca tablicę dynamiczną
- posiada dwa argumenty:
    - pierwszy z nich to tablica dynamiczna (źródło)
    - drugi z nich to druga tablica dynamiczna (wartości)
- utworzenie drugiej tablicy za pomocą funkcji z zadania 1, max 10 elementów
- zadanie polega na wyszukaniu indeksu w tablicy wartości z drugiej tablicy
- jeśli tablica źródłowa nie posiada elementu to wpisz -1,
- zwracamy indexy, 

---

## Zadanie_3
- Generujemy dane z zadania_1 -> 1024
- Wyszukujemy wszystkie unikalne wartości
- sortujemy od max do min

---
- zliczamy ile razy powtórzyły się liczby (Pan od programowani)


---
branch -> sci_po_task_01
folder -> sci_po_task_01
tylko plik main.cpp
