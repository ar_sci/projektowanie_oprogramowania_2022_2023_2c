# sci_projektowanie_oprogramowania_2022_2023_2c

Lista zadań:
1. Zliczenie słów za pomocą `count_if`
1. Znajdź element za pomocą `find_if`
1. Sortowanie i wyświetlanie
1. all_of, any_of, none_of

***
Kod źródłowy zawiera generację danych.

```c++
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

const std::vector<std::string> words = {
    "dog", "tiger", "count", "cereal", "remember", "X-ray", "stomach", "climate", "pyramid", "exclude", "lineage", "deserve", "verdict", "racism", "update", "source", "cottage", "exact", "bald", "preach", "master", "overview", "climb", "promote", "stereotype", "coerce", "decay", "superintendent", "write", "jet", "do", "overall", "youth", "fountain", "shop", "tax", "cunning", "consider", "trust", "heel", "reduction", "wagon", "carve", "variant", "ordinary", "archive", "work", "wedding", "hover", "drill",
    "series", "complain", "drawing", "photocopy", "funeral", "depression", "tray", "repeat", "emphasis", "wage", "agony", "function", "thigh", "wheel", "passage", "budge", "school", "zone", "outer", "presence", "cream", "torture", "market", "clear", "offset", "degree", "crutch", "census", "gene", "adventure", "toast", "to", "build", "contrary", "dedicate", "hurt", "test", "past", "despise", "lazy", "certain", "ambiguous", "kinship", "attention", "return", "glance", "tail", "minister", "assignment", "ordinary",
    "consider", "filter", "weed", "judge", "profit", "trick", "define", "period", "launch", "umbrella", "reliance", "straighten", "business", "unfortunate", "cottage", "tiger", "sound", "generation", "scramble", "shop", "useful", "manufacturer", "wound", "onion", "jealous", "color-blind", "rung", "degree", "few", "licence", "quality", "dish", "remark", "spit", "addicted", "slow", "connection", "parking", "breathe", "will", "nut", "row", "rain", "hen", "detective", "sick", "bait", "digress", "mail", "embryo",
};

struct data_t {
    std::size_t id;
    std::string word;
    std::size_t number;
};

int main(int, char* argv[])
{
    srand(44338);
    std::vector<data_t> data;

    for (std::size_t i = 0; i < 100000; ++i) {
        data.push_back({
            i, words[rand() % words.size()], static_cast<std::size_t>(rand()),
        });
    }
    
    return 0;
}
```

***

### Zadanie_1
- za pomocą funkcji `count_if`, zlicz ilość występowanych słów w tablicy dynamicznej
    - `tray`
    - `business`
    - `budge`

### Zadanie_2
- za pomocą funkcji `find_if` znajdź następujące elementy:
    - nazwa: `X-ray` **i** id: większe niż 25000
    - nazwa: `trick` **i** id: większe niż 45000
- wyświetl informacje o id oraz indeksie
- indeks wylicz za pomocą funkcji `std::distance`

### Zadanie_3
- za pomocą funkcji `sort`, posortuj elementy w tablicy dynamicznej, tak aby wartości były posortowane za pomocą tekstu
- wyświetl pierwsze 10 elementów, po posortowaniu

### Zadanie_4
- utwórz własne tablice dynamiczne, zawierające pewne dane (np. liczby)
- wykorzystaj następujące funkcje w celu sprawdzenia co one realizują:
    - all_of, any_of, none_of

***

Odpowiedzi:
```
Zadanie_1:
    sum1: 669 sum2: 661 sum3: 645

Zadanie_2:
    id: 25273
    index: 25273
    id: 45163
    index: 45163

Zadanie_3
    id: 7487 word: X-ray number: 25941
    id: 64002 word: X-ray number: 526
    id: 97615 word: X-ray number: 30432
    id: 64486 word: X-ray number: 29114
    id: 91779 word: X-ray number: 29337
    id: 8967 word: X-ray number: 21483
    id: 10605 word: X-ray number: 22641
    id: 79556 word: X-ray number: 13681
    id: 76571 word: X-ray number: 17270
    id: 72296 word: X-ray number: 23286
```
