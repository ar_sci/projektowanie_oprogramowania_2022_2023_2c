#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

int main(int /*argc*/, char* /*argv*/[])
{
    sf::RenderWindow render_window {
        sf::VideoMode{
            1280,
            720,
        },
        sf::String{ "sci_po_project" },
    };

    bool is_running = true;
    sf::Clock clock;
    do {
        sf::Event event;
        while (render_window.pollEvent(event)) {
            if (sf::Event::EventType::Closed == event.type) {
                is_running = false;
            }
        }

        const sf::Time delta_time = clock.restart();

        render_window.clear();
        render_window.display();
    } while (is_running);

    return 0;
}
