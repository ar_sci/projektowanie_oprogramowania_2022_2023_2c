﻿#include <iostream>

#include <SFML/Graphics.hpp>

int main(int argc, char* argv[])
{
    sf::RenderWindow render_window{ sf::VideoMode{ 1280, 800 }, "SCI_PO_SFML_PROJECT" };

    sf::Texture sci_logo;
    sci_logo.loadFromFile("./assets/sci_logo.png");

    sf::Sprite sci_logo_sprite;
    sci_logo_sprite.setTexture(sci_logo);
    sci_logo_sprite.setOrigin(sf::Vector2f{ 44.0f, 50.0f });
    sci_logo_sprite.setPosition(sf::Vector2f{ 640.0f, 400.0f });

    const float rotation_angle_speed = 360.0f * 0.2f;

    sf::Clock clock;
    bool is_running = true;
    do {
        sf::Event event;
        while (render_window.pollEvent(event)) {
        }

        const sf::Time delta_time = clock.restart();
        const float delta_time_seconds = delta_time.asSeconds();


        float sci_logo_rotation = sci_logo_sprite.getRotation();
        sci_logo_rotation += delta_time_seconds * rotation_angle_speed;
        sci_logo_sprite.setRotation(sci_logo_rotation);


        render_window.clear();
        render_window.draw(sci_logo_sprite);
        render_window.display();
    } while (is_running);

    return 0;
}
