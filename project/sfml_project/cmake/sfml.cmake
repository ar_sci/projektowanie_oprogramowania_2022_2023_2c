include(ExternalProject)

ExternalProject_Add(sfml
    PREFIX third_party
    GIT_REPOSITORY "https://github.com/SFML/SFML"
    GIT_TAG "2.5.1"
    INSTALL_COMMAND ""
    UPDATE_COMMAND ""
    CMAKE_ARGS
        -DSFML_BUILD_AUDIO=OFF
        -DSFML_BUILD_NETWORK=OFF
        -DCMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG=${CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG}
        -DCMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE=${CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE}
        -DCMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG=${CMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG}
        -DCMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE=${CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE}
        -DCMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG=${CMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG}
        -DCMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE=${CMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE}
)

set(SFML_INCLUDE_DIR ${CMAKE_BINARY_DIR}/third_party/src/SFML/include)
set(SFML_LIBRARIES
    $<IF:$<CONFIG:Debug>,sfml-main-d.lib,sfml-main.lib>
    $<IF:$<CONFIG:Debug>,sfml-system-d.lib,sfml-system.lib>
    $<IF:$<CONFIG:Debug>,sfml-window-d.lib,sfml-window.lib>
    $<IF:$<CONFIG:Debug>,sfml-graphics-d.lib,sfml-graphics.lib>
)
