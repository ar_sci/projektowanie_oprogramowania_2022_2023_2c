## Napisz program w języku C++, który wykorzystuje szablony do stworzenia funkcji szukającej największej wartości w tablicy liczb całkowitych oraz najmniejszej wartości w tablicy liczb zmiennoprzecinkowych.

Specyfikacja:
1. Program powinien zacząć od stworzenia dwóch tablic: jednej zawierającej liczby całkowite, drugiej - liczby zmiennoprzecinkowe.
1. Następnie program powinien wykorzystać szablony do stworzenia funkcji szukającej największą wartość w tablicy liczb całkowitych oraz najmniejszą wartość w tablicy liczb zmiennoprzecinkowych.
1. Funkcje powinny zostać wywołane z odpowiednimi parametrami (czyli tablicami stworzonymi w kroku 1).
1. Program powinien wyświetlić wyniki w postaci "Największa liczba całkowita: [wynik]" oraz "Najmniejsza liczba zmiennoprzecinkowa: [wynik]".

---

## Napisz program w języku C++, który wykorzystuje szablony do stworzenia funkcji sumującej elementy tablicy o różnych typach danych.

Specyfikacja:
1. Program powinien zacząć od stworzenia trzech tablic: jednej zawierającej liczby całkowite, drugiej - liczby zmiennoprzecinkowe, a trzeciej - ciągi znaków.
1. Następnie program powinien wykorzystać szablon do stworzenia funkcji sumującej elementy w tablicy. Funkcja powinna działać dla różnych typów danych, dlatego powinna być zdefiniowana jako szablon.
1. Funkcja powinna zostać wywołana z odpowiednimi parametrami (czyli tablicami stworzonymi w kroku 1).
1. Program powinien wyświetlić wynik w postaci "Suma elementów w tablicy: [wynik]".

---

## Napisz program w języku C++, który wykorzystuje szablony do stworzenia funkcji, która znajduje wszystkie elementy w tablicy o podanej wartości.

Specyfikacja:
1. Program powinien zacząć od stworzenia dwóch tablic: jednej zawierającej liczby całkowite, drugiej - liczby zmiennoprzecinkowe.
1. Następnie program powinien wykorzystać szablony do stworzenia funkcji szukającej wszystkie elementy w tablicy o podanej wartości. Funkcja powinna zwracać wektor indeksów (liczba całkowita) wszystkich elementów w tablicy, które mają wartość równą szukanej wartości.
1. Funkcja powinna zostać wywołana z odpowiednimi parametrami (czyli tablicami stworzonymi w kroku 1 i wartością, którą chcemy znaleźć).
1. Program powinien wyświetlić wyniki w postaci "Znalezione indeksy: [indeksy]" (gdzie [indeksy] to wektor indeksów znalezionych elementów).

---

## Napisz program w języku C++, który wykorzystuje szablony do stworzenia funkcji sortującej elementy tablicy.

Specyfikacja:
1. Program powinien zacząć od stworzenia dwóch tablic: jednej zawierającej liczby całkowite, drugiej - liczby zmiennoprzecinkowe.
1. Następnie program powinien wykorzystać szablony do stworzenia funkcji sortującej elementy w tablicy. Funkcja powinna działać dla różnych typów danych, dlatego powinna być zdefiniowana jako szablon.
1. Funkcja powinna zostać wywołana z odpowiednimi parametrami (czyli tablicami stworzonymi w kroku 1).
1. Program powinien wyświetlić wynik w postaci posortowanej tablicy.

---
