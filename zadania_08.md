# # Zadanie_08 - kontenery
## Zadania
```
Napisz program w języku programowania C++:
- [ ] Zadanie_0: Utworzenie tablicy i wypełnienie jej losowymi danymi.
- [ ] Zadanie_1: Utworzenie tablicy dynamicznej.
- [ ] Zadanie_2: Zastosowanie tablicy asocjacyjnej (klucz/wartość).
- [ ] Zadanie_3: Zastosowanie zbioru.
- [ ] Zadanie_4: Zapisanie do pliku.
```

## Zadanie_0 - szczegóły
- utwórz tablicę o stałym rozmiarze (`std::array`, typ `unsigned int`)
- tablica ma mieć rozmiar 256 elementów
- utwórz pętlę (`range loop`, pamiętaj aby zrobić referencję), która wypełni losowymi danymi
- ziarno do losowania ustaw na 138
- przedział liczb od 16 do 64 (włącznie, [16; 64])

## Zadanie_1 - szczegóły
- utwórz tablicę o dynamicznym rozmiarze (`std::vector`, typ `unsigned int`)
- z tablicy z zadania nr_0 wpisz tylko unikalne wartości do tablicy dynamicznej
- posortuj od największej do najmniejszej
- utwórz drugą tablicę o dynamicznym rozmiarze (`std::vector`, typ `std::string`)
- wygeneruj ciągi znaków, które posiadają wylosowane literki, a długość ciągu jest równa wartości z tablicy dynamicznej

## Zadanie_2 - szczegóły
- operacja pętli, którą należy wykonać na zbiorze danych z zadania nr_1
- w pęli, utwórz tablicę asocjacyjną (std::map, klucz: char, wartość: unsigned int)
- pobierz dane z wektora, będzie to jeden string (tekst)
- należy utworzyć pętlę, która pozwoli wyciągnać jeden znak
    - ten znak jest kluczem, który posłuży do sprawdzenia wartości w słowniku
    - jeśli występuje, to zwiększamy element o jeden, jeśli nie, to przypisujemy nową wartość (1)
- pętla umożliwiła, zliczenie znaków w ciągu znaków
- następnie utwórz kolejną, która będzie bazować na (range loop), tak aby mieć możliwość sprawdzenia, który element wystąpił najwięcej razy

## Zadanie_3 - szczegóły
- utwórz zbiór (std::set, typ unsigned int)
- wprowadź do zbioru, liczby, które zostały wygenerowane w zadaniu nr_0
- wyświetl wynik na konsoli

## Zadanie_4 - szczegóły
- utwórz zmienną, która umożliwi tobie zapisywanie danych do pliku
- umieść w dopowiednich miejscach możliwość zapisu wyników do pliku
- wzór znajduje się w pliku: [zadania_8_wynik.txt](https://gitlab.com/ar_sci/projektowanie_oprogramowania_2022_2023_2c/-/blob/main/assets/zadania_08_wynik.txt)
