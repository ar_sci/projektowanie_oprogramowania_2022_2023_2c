
1. Wczytaj plik: `exam_00_data.txt`
1. Utwórz tablicę dynamiczną, która posiada wpisy z pliku (za pomocą struktury)
1. Wykorzystaj wartości wczytane z pliku do następującego algorytmu
	- `liczba_a + liczba_b`
1. Wyniki zapamiętaj do drugiej tablicy dynamicznej.
1. Wynik zapisz do pliku: `exam_00_result_0.txt`.
1. Z tablicy wyników należy znaleźć wszystkie unikalne wartości (mogą być posortowane, od min do max)
1. Wynik zapisz do pliku: `exam_00_result_1.txt`.
1. Z tablicy wyników należy ile dana wartość się powtarza.
1. Wynik zapisz do pliku: `exam_00_result_2.txt`.

