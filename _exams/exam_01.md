1. Plik `exam_01_data.txt` zawiera 200 wpisów. Wpis składa się tylko z małych liter alfabetu angielskiego. Długość wpisu wynosi od 3 do 10 znaków.
1. Wczytaj plik: `exam_01_data.txt`.
1. Podaj ile wpisów posiada parzystą i nie parzystą ilość znaków. Zapisz wynik do pliku `exam_01_result_a.txt`.
1. Utwórz listę wpisów, które są palindromami. `Palindrom to wyraz brzmiący tak samo przy czytaniu z lewej strony do prawej, jak i odwrotnie, np. kajak, potop`. Zapisz wynik do pliku `exam_01_result_b.txt`.
1. Utwórz listę wpisów, które zawierają w sobie dwa kolejne znaki, których suma kodów ASCII wynosi 220. Zapisz wynik do pliku `exam_01_result_c.txt`. Do pliku zapisz następująco `wpis` oraz dwie literki, które spełniają założenie.
