#include <fstream>

#include <iostream>

#include <map>
#include <vector>

#include <string>
#include <sstream>

struct awaria_entry_t
{
    int numer_zgloszenia;
    int numer_komputera;
    std::string czas_awarii;
    int priorytet;
};

struct komputer_entry_t
{
    char sekcja;
    int pojemnosc_dysku;
};

struct naprawa_entry_t
{
    int numer_zgloszenia;
    std::string czas_naprawy;
    std::string rodzaj;
};

namespace utils {
    std::vector<std::string> split(const std::string& str, char delimiter);
    std::vector<std::string> load_file(const std::string& filename);
}

namespace initialization {
    std::vector<awaria_entry_t> load_awarie();
    std::map<int, komputer_entry_t> load_komputery();
    std::vector<naprawa_entry_t> load_naprawy();
}

int main()
{
    const std::vector<awaria_entry_t> awarie = initialization::load_awarie();
    const std::map<int, komputer_entry_t> komputery = initialization::load_komputery();
    const std::vector<naprawa_entry_t> naprawy = initialization::load_naprawy();

#if 0
    {
        // https://en.cppreference.com/w/cpp/container/vector
        // https://cplusplus.com/reference/vector/vector/
        // https://cpp0x.pl/kursy/Kurs-STL-C++/Kontener-tablicy-std-vector/119
        // https://www.geeksforgeeks.org/vector-in-cpp-stl/
        // range loop dla `std::vector`
        for (const awaria_entry_t& entry : awarie) {
            std::cout << entry.czas_awarii << std::endl;
        }
        // zwykła pętla dla `std::vector`
        for (std::size_t index = 0; index < awarie.size(); ++index) {
            const awaria_entry_t& entry = awarie[index];
            std::cout << entry.czas_awarii << std::endl;
        }
    }
    {
        // https://en.cppreference.com/w/cpp/container/map
        // https://cplusplus.com/reference/map/map/
        // https://cpp0x.pl/kursy/Kurs-C++/Poziom-5/Kontenery-asocjacyjne-std-set-i-std-map/589
        // https://www.geeksforgeeks.org/map-associative-containers-the-c-standard-template-library-stl/
        // 
        // range loop dla `std::map`
        // nazwy `key` i `value` mogą być inne
        // dla tego przypadku:
        //      key   -> liczba           => numer_komputera
        //      value -> struktura danych => komputer_entry_t
        for (const auto& [key, value] : komputery) {
            std::cout << key << " " << value.pojemnosc_dysku << std::endl;
        }
        for (const auto& [numer_komputera, entry] : komputery) {
            std::cout << numer_komputera << " " << entry.pojemnosc_dysku << std::endl;
        }
    }
#endif

    return 0;
}

namespace utils {
    std::vector<std::string> split(const std::string& str, char delimiter)
    {
        std::vector<std::string> result;

        std::stringstream ss{str};
        std::string item;
        while (std::getline(ss, item, delimiter)) {
            result.push_back(item);
        }

        return result;
    }

    std::vector<std::string> load_file(const std::string& filename)
    {
        std::vector<std::string> result;

        std::fstream file;
        file.open(filename, std::ios::in);

        if (file.is_open()) {
            while (!file.eof()) {
                std::string line;
                std::getline(file, line);
                result.push_back(line);
            }
        }

        return result;
    }
}

namespace initialization {
    std::vector<awaria_entry_t> load_awarie()
    {
        std::vector<awaria_entry_t> result;

        const std::vector<std::string> data = utils::load_file("awarie.txt");
        // pomijamy pierwsza linie, poniewaz jest to naglowek pliku
        // pomijamy ostatnia linie, poniewaz jest to pusta linia
        for (std::size_t i = 1; i < data.size() - 1; ++i) {
            // linia sklada sie z kilku kolumn, ktore sa rozdzielone spacjami
            const std::string& line = data[i];
            // funkcja `split` dzieli jedna linie na kilka elementow, ktore znajduja sie w tablicy dynamicznej
            const std::vector<std::string> elements = utils::split(line, '\t');

            // zapomocą funkcji `std::stoi`, masz możliwość zamiany `std::string` na `int`
            // jeśli wartością typu `std::string` nie są tylko cyfry, to może powodować to błąd
            // można utworzyć funkcję, która sprawdza, czy zawartość składa się z cyfr

            awaria_entry_t entry;
            entry.numer_zgloszenia = std::stoi(elements[0]);
            entry.numer_komputera = std::stoi(elements[1]);
            entry.czas_awarii = elements[2];
            entry.priorytet = std::stoi(elements[3]);

            result.push_back(entry);
        }

        return result;
    }

    std::map<int, komputer_entry_t> load_komputery()
    {
        std::map<int, komputer_entry_t> result;

        const std::vector<std::string> data = utils::load_file("komputery.txt");
        for (std::size_t i = 1; i < data.size() - 1; ++i) {
            const std::string& line = data[i];
            const std::vector<std::string> elements = utils::split(line, '\t');

            int numer_komputera = std::stoi(elements[0]);

            komputer_entry_t entry;
            entry.sekcja = elements[1][0]; // Tutaj potrzebujemy tylko literki, więc `std::string` to tablica znaków, więc pod odpowiednim indeksem znajduje się literka
            entry.pojemnosc_dysku = std::stoi(elements[2]);

            result.insert({ numer_komputera, entry });
        }

        return result;
    }

    std::vector<naprawa_entry_t> load_naprawy()
    {
        std::vector<naprawa_entry_t> result;

        const std::vector<std::string> data = utils::load_file("naprawy.txt");
        for (std::size_t i = 1; i < data.size() - 1; ++i) {
            const std::string& line = data[i];
            const std::vector<std::string> elements = utils::split(line, '\t');

            naprawa_entry_t entry;
            entry.numer_zgloszenia = std::stoi(elements[0]);
            entry.czas_naprawy = elements[1];
            entry.rodzaj = elements[2];

            result.push_back(entry);
        }

        return result;
    }
}
